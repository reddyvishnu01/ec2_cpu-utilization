import boto3
from datetime import datetime, timedelta


def get_cpu_util(instanceID,region):

    cw = boto3.client('cloudwatch',region_name=region)
    response = cw.get_metric_statistics(
        Namespace='AWS/EC2',
        MetricName='CPUUtilization',
        Dimensions=[
            {
                'Name': 'InstanceId',
                'Value': instanceID
            },
        ],
        StartTime=datetime.now() - timedelta(hours=24),
        EndTime=datetime.now(),
        Period=86400,
        Statistics=[
            'Average'
        ],
        Unit='Percent'
    )
    for values in response['Datapoints']:
         return values['Average']
