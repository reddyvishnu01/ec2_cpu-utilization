import boto3
import EC2_CPUUtilisation
import csv
from datetime import datetime

ec2 = boto3.client('ec2')

ec2_regions = [region['RegionName'] for region in ec2.describe_regions()['Regions']]

result = [["AWS_RegionName", "InstanceID", "Avg.CPU(Percenatge)"]]

for region in ec2_regions:
    ec2 = boto3.resource('ec2', region_name=region)
    instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])

    for instance in instances:
        cpu_avg = EC2_CPUUtilisation.get_cpu_util(instance.id,region)
#        print(region, instance.id, result)
        result.append([region, instance.id, cpu_avg])
#print(result)
#path = "results/result-" + datetime.now().strftime("%Y%m%d") + ".csv"
path = "results/result.csv"
with open(path, 'w') as resultfile:
    writer = csv.writer(resultfile, lineterminator='\n')
    writer.writerows(result)
resultfile.close()

